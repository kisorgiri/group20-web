import { toast } from 'react-toastify';

function showSuccess(msg) {
    toast.success(msg);
}

function showWarning(msg) {
    toast.warn(msg);
}
function showInfo(msg) {
    toast.info(msg);
}

function showErr(errMsg = "something went wrong") {
    toast.error(errMsg);
}



function handleError(err) {
    debugger;
    if (err && err.data) {
        showErr(err.data.msg);
    }
    // error can be anything
    // check what comes in err
    // parse the error message 
    // show appropriate message
    // showErr();

}

export default {
    showSuccess,
    showInfo,
    showWarning,
    handleError
}



