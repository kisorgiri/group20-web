import axios from 'axios';
const BaseURL = process.env.REACT_APP_BASE_URL;
const http = axios.create({
    baseURL: BaseURL,
    responseType: 'json'
})

const getHeaders = (secure) => {
    let headerOptions = {
        'Content-Type': 'application/json'
    }
    if (secure) {
        headerOptions['Authorization'] = localStorage.getItem('token')

    }
    return headerOptions;
}

function GET(url, secure = false, params) {
    return http.get(url, {
        headers: getHeaders(secure),
        params: params
    });

}
function POST(url, data, secure = false) {
    return http
        .post(
            url,
            data,
            {
                headers: getHeaders(secure),
                params: {}
            })
}
function PUT(url, data, secure = true) {
    return http
        .put(
            url,
            data,
            {
                headers: getHeaders(secure),
                params: {}
            }
        )
}
function REMOVE(url, secure = true) {
    return http.delete(
        url,
        {
            headers: getHeaders(secure)
        }
    )
}

function upload(method, url, files, data, secure = true) {
    return new Promise(function (resolve, reject) {
        const xhr = new XMLHttpRequest();
        const formData = new FormData();
        if (files.length) {
            formData.append('img', files[0], files[0].name);
        }
        // deleting property of object
        delete data.new_image;

        for (let item in data) {
            formData.append([item], data[item]);
        }
        xhr.onreadystatechange = () => {
            // logic behind xhr request

            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    resolve(xhr.response)
                } else {
                    reject(xhr.response);
                }
            }
        }
        xhr.open(method, url, true);
        xhr.send(formData);
    })


}

export default {
    GET,
    POST,
    PUT,
    DELETE: REMOVE,
    upload
}
