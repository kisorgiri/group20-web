import React from 'react';
import ReactDOM from 'react-dom';
import { App } from './app';

const mainDiv = document.getElementById('root');

ReactDOM.render(<App />, mainDiv);