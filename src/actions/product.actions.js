// function fetch_product(args, ) {
//     return function (dispatch) {

//     }
// }
import { SET_IS_LOADING, GET_PRODUCTS, SET_PAGE_NUMBER } from './types';
import httpClient from '../utils/httpClient';
import notification from '../utils/notification';

export const fetch_product = (pageNumber = 1, pageSize = 5) => (dispatch) => {
    // http call
    dispatch(loading(true));
    console.log('calling BE');

    httpClient.GET('/product', true, {
        pageNumber,
        pageSize
    })
        .then(data => {
            console.log('data in actions >>', data);
            dispatch({
                type: GET_PRODUCTS,
                payload: data.data
            })
        })
        .catch(err => {
            notification.handleError(err.response);
        })
        .finally(() => {
            dispatch(loading(false));
        });

}

export const loading = (isLoading) => ({
    type: SET_IS_LOADING,
    payload: isLoading
})

export const setPageNumber = (pageNumber) => ({
    type: SET_PAGE_NUMBER,
    payload: pageNumber
})
