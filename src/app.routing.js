import React from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import NavBar from './components/common/navBar/navBar.component';
import SideBar from './components/common/sideBar/sideBar.component';
import { Login } from './components/login/login.component';
import { Register } from './components/register/register.component';
import { NotFound } from './components/common/pageNotFound/pagenotfound.component';
import { Dashboard } from './components/dashboard/dashboard.component';
import { ViewProduct } from './components/products/view-products/view-products.component';
import { EditProduct } from './components/products/edit-product/edit-product';
import { AddProduct } from './components/products/add-product/add-product.component';
import { ForgotPassword } from './components/forgot-password/forgot-password.component';
import { ResetPassword } from './components/reset-password/reset-password.component';
import SearchProduct from './components/products/search-products/search-product';
import MessageComponent from './components/messages/message.component';

const ProctectedRoute = ({ component: Component, ...props }) => {
    return (
        <Route {...props} render={(props) => (
            localStorage.getItem('token')
                ? <>
                    <div className="header">
                        <NavBar isLoggedIn={true}></NavBar>
                    </div>
                    <div className="sidebar">
                        <SideBar></SideBar>
                    </div>
                    <div className="content">
                        <Component {...props}></Component>
                    </div>
                </>
                : <Redirect to="/" ></Redirect> // TODO send props

        )} />
    )

}

const PublicRoute = ({ component: Component, ...props }) => {
    return (
        <Route {...props} render={(props) => (
            <>
                <div className="header">
                    <NavBar isLoggedIn={localStorage.getItem('token') ? true : false}></NavBar>
                </div>
                <div className="content">
                    <Component {...props}></Component>
                </div>
            </>
        )} />
    )

}

const AppRoutes = () => {
    return (
        <Router>
            <Switch>
                <PublicRoute exact path="/" component={Login}></PublicRoute>
                <PublicRoute path="/register" component={Register}></PublicRoute>
                <PublicRoute path="/forgot-password" component={ForgotPassword}></PublicRoute>
                <PublicRoute path="/reset-password/:token" component={ResetPassword}></PublicRoute>
                <PublicRoute path="/search-product" component={SearchProduct}></PublicRoute>
                <ProctectedRoute path="/dashboard" component={Dashboard}></ProctectedRoute>
                <ProctectedRoute path="/add-product" component={AddProduct}></ProctectedRoute>
                <ProctectedRoute path="/view-product" component={ViewProduct}></ProctectedRoute>
                <ProctectedRoute path="/edit-product/:id" component={EditProduct}></ProctectedRoute>
                <ProctectedRoute path="/messages" component={MessageComponent}></ProctectedRoute>

                <PublicRoute component={NotFound}></PublicRoute>
            </Switch>

        </Router>
    )
}

export default AppRoutes;


        // revision
        // react-router-dom
        // BrowserRouter
        // Route configuration block to add routing config
        // Route must be wrapped insdie BrowserRouter
        // exact, is attribute for Route
        // Switch for loading only one component
        // default Component as 404

        //Link is used in templates to navigate
// {/* <Link to="/address"></Link> */}

// component navigation using props

// each and every component which is wrapped inside router has  3 different props
// history, match , location

// history.push('/') navigation 


// if we have dynamic endpoint config
// dynamic value will be available inside match >> params
