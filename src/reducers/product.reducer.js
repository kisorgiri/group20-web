import { SET_IS_LOADING, GET_PRODUCTS, SET_PAGE_NUMBER } from './../actions/types';

const initialState = {
    isLoading: false,
    products: [],
    pageNumber: 1,
    pageSize: 5
}
function reducer(state = initialState, action) {
    console.log('here at reducers >>>', action);
    switch (action.type) {
        case GET_PRODUCTS:
            return {
                ...state,
                products: action.payload
            };
        case SET_IS_LOADING:
            return {
                ...state,
                isLoading: action.payload
            }
        case SET_PAGE_NUMBER:
            return {
                ...state,
                pageNumber: action.payload
            }

        default:
            return state;
    }

}

export default reducer;