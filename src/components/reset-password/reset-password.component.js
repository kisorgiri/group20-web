import React, { Component } from 'react'
import httpClient from '../../utils/httpClient';
import notification from '../../utils/notification';

export class ResetPassword extends Component {
    constructor() {
        super();
        this.state = {
            isSubmitting: false,
            password: null,
            confirmPassword: null
        }
    }
    componentDidMount() {
        this.userId = this.props.match.params.token;
    }

    handleChange = e => {
        const { name, value } = e.target;
        this.setState({
            [name]: value
        });
    }
    handleSubmit = e => {
        e.preventDefault();
        this.setState({
            isSubmitting: true
        })
        httpClient.POST('/auth/reset-password/' + this.userId, { password: this.state.password })
            .then((data) => {
                notification.showInfo('Password reset successfull please login')
                this.props.history.push('/');
            })
            .catch(err => {
                notification.handleError(err.response);
            })
            .finally(() => {
                this.setState({
                    isSubmitting: false
                })
            })
    }
    render() {
        let btn = this.state.isSubmitting
            ? <button className="btn btn-info" disabled={true}>submitting...</button>
            : <button type="submit" className="btn btn-primary" >submit</button>
        return (
            <>
                <h2>Reset Password</h2>
                <form className="form-group" onSubmit={this.handleSubmit}>
                    <label>Password</label>
                    <input className="form-control" type="password" placeholder="Password" name="password" onChange={this.handleChange}></input>
                    <label>Confirm Password</label>
                    <input className="form-control" type="password" placeholder="Confirm Password" name="confirmPassword" onChange={this.handleChange}></input>
                    <br />
                    {btn}
                </form>
            </>
        )
    }
}
