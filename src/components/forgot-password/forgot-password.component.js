import React, { Component } from 'react'
import httpClient from '../../utils/httpClient';
import notification from '../../utils/notification';

export class ForgotPassword extends Component {
    constructor() {
        super();
        this.state = {
            isSubmitting: false,
            email: null
        }
    }

    handleChange = e => {
        const { name, value } = e.target;
        this.setState({
            [name]: value
        });
    }
    handleSubmit = e => {
        e.preventDefault();
        this.setState({
            isSubmitting: true
        })
        httpClient.POST('/auth/forgot-password', { email: this.state.email })
            .then((data) => {
                notification.showInfo('Password reset link sent to your email please check your inbox')
                this.props.history.push('/');
            })
            .catch(err => {
                notification.handleError(err.response);
            })
            .finally(() => {
                this.setState({
                    isSubmitting: false
                })
            })
    }
    render() {
        let btn = this.state.isSubmitting
            ? <button className="btn btn-info" disabled={true}>submitting...</button>
            : <button type="submit" className="btn btn-primary" >submit</button>
        return (
            <>
                <h2>Forgot Password</h2>
                <form className="form-group" onSubmit={this.handleSubmit}>
                    <label>Email</label>
                    <input className="form-control" type="email" placeholder="email address" name="email" onChange={this.handleChange}></input>
                    <br />
                    {btn}
                </form>
            </>
        )
    }
}
