import React from 'react';
import './navBar.component.css';
import { Link, withRouter } from 'react-router-dom';
// functional component

const logout = (props) => {
    // redirec to login page
    // clear localstorage once logged out
    localStorage.clear();
    props.history.push("/");

}
const NavBar = (props) => {
    // it must return html node
    const navBarContent = props.isLoggedIn
        ? <div className="navBar">
            <ul className="nav-list">
                <li className="nav-item">
                    <Link to='/dashboard'>Home</Link>

                </li>
                <li className="nav-item">
                    <Link to='/profile'> Profile</Link>

                </li>
                <li className="nav-item">
                    <button className="btn btn-success" onClick={() => logout(props)}>logout</button>
                </li>
            </ul>
        </div>
        : <div className="navBar">
            <ul className="nav-list">
                <li className="nav-item">
                    <Link to='/register'> Register</Link>
                </li>
                <li className="nav-item">
                    <Link to='/'>Login</Link>
                </li>
            </ul>
        </div>

    return (
        navBarContent
    )
}

export default withRouter(NavBar);