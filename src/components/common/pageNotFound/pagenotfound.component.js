import React from 'react';

export const NotFound = () => {
    return (
        <>
            <h2>Not Found</h2>
            <img src="/images/download.jpeg" alt="notfound.png" width="600px"></img>
        </>
    )
}