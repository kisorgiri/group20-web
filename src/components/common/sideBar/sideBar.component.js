import React from 'react'
import './sideBar.component.css';
import { Link } from 'react-router-dom';

export default function SideBar() {
    return (
        <div>
            <ul className="sidebar-menu">
                <li className="sidebar-item">
                    <Link to="/dashboard">Home</Link>
                </li>
                <li className="sidebar-item">
                    <Link to="/add-product">Add Product</Link>
                </li>
                <li className="sidebar-item">
                    <Link to="/view-product">View Product</Link>
                </li>
                <li className="sidebar-item">
                    <Link to="/search-product">Search Product</Link>
                </li>
                <li className="sidebar-item">
                    <Link to="/messages">Messages</Link>
                </li>
                <li className="sidebar-item">
                    <Link to="/notification">Notification</Link>
                </li>

            </ul>
        </div>
    )
}
