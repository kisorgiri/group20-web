import React, { Component } from 'react'
import httpClient from '../../../utils/httpClient';
import notification from '../../../utils/notification';
import { ProductForm } from './../product-form/product-form';
import { Loader } from './../../common/loader/loader.component';

export class EditProduct extends Component {
    constructor() {
        super();
        this.state = {
            isLoading: false,
            isSubmitting: false,
            data: {}
        };
    }    

    componentDidMount() {
        this.setState({
            isLoading: true
        })
        this.productId = this.props.match.params.id;
        httpClient.GET(`/product/${this.productId}`, true)
            .then(data => {
                this.setState({
                    data: data.data
                })
            })
            .catch(err => {
                notification.handleError(err);
            })
            .finally(() => {
                this.setState({
                    isLoading: false
                })
            })
    }
    render() {
        let content = this.state.isLoading
            ? <Loader></Loader>
            : <div>
                <ProductForm title="Edit Product" productData={this.state.data} ></ProductForm>
            </div>
        return (
            <>
                {content}
            </>
        )
    }
}
