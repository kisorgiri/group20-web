import React, { Component } from 'react';
import http from './../../../utils/httpClient';
import notification from '../../../utils/notification';
import { withRouter } from 'react-router';

const IMG_URL = process.env.REACT_APP_IMG_URL;
const BaseURL = process.env.REACT_APP_BASE_URL;
const defaultForm = {
    name: '',
    category: '',
    description: '',
    brand: '',
    color: '',
    price: '',
    manuDate: '',
    expiryDate: '',
    discountedItem: '',
    discountType: '',
    discount: '',
    image: '',
    new_image: '',
    tags: ''
}
class ProductFormComponent extends Component {

    constructor() {
        super();
        this.state = {
            data: {
                ...defaultForm
            },
            error: {
                ...defaultForm
            },
            isSubmitting: false,
            isValidForm: false
        }
        this.url = `${BaseURL}/product?token=${localStorage.getItem('token')}`;
    }

    handleChange = e => {
        let { name, value, type, checked } = e.target;
        if (type === 'checkbox') {
            value = checked
        }
        if (type === 'file') {
            value = e.target.files
        }
        this.setState((pre) => ({
            data: {
                ...pre.data,
                [name]: value

            }
        }), () => {
            // form validation logic here make different function
        })
    }

    componentDidMount() {
        if (this.props.productData) {
            let data = this.props.productData;
            data.discountedItem = this.props.productData.discount && this.props.productData.discount.discountedItem
                ? true
                : false;
            data.discountType = this.props.productData.discount
                && this.props.productData.discount.discountType
                ? this.props.productData.discount.discountType
                : '';
            data.discount = this.props.productData.discount
                && this.props.productData.discount.discount
                ? this.props.productData.discount.discount
                : '';
            data.price = this.props.productData.price || 0;
            this.setState({

                data: {
                    ...defaultForm,
                    ...data
                }
            })
        }

    }

    handleSubmit = e => {
        this.setState({
            isSubmitting: true
        })
        e.preventDefault();

        if (this.state.data._id) {
            this.editProduct(this.state.data._id, this.state.data);
        }
        else {
            this.addProduct(this.state.data);
        }

    }

    addProduct(data) {
        http
            .upload('POST', this.url, data.new_image, data)
            .then(data => {
                notification.showInfo('Product added successfully');
                this.props.history.push('/view-product');
            })
            .catch(err => {
                notification.handleError(err.response);
                this.setState({
                    isSubmitting: false
                })
            })

    }

    editProduct(id, data) {
        http
            .PUT(`/product/${id}`, data, true)
            .then(data => {
                notification.showInfo('Product Updated successfully');
                this.props.history.push('/view-product');
            })
            .catch(err => {
                notification.handleError(err.response);
                this.setState({
                    isSubmitting: false
                })
            })
    }

    render() {
        let discountContent = this.state.data.discountedItem
            ? <>
                <br></br>
                <label>Discount Type</label>
                <input className="form-control" value={this.state.data.discountType} type="text" name="discountType" placeholder="discountType" onChange={this.handleChange}></input>
                <label>Discount Value</label>
                <input className="form-control" value={this.state.data.discount} type="text" name="discount" placeholder="Discount Value" onChange={this.handleChange}></input>
            </>
            : '';

        let btn = this.state.isSubmitting
            ? <button className="btn btn-info" disabled={true}>submitting...</button>
            : <button className="btn btn-primary" type="submit">submit</button>

        let preImage = this.state.data.image
            ? <>
                <p>Previous Image</p>
                <img src={`${IMG_URL}/${this.state.data.image}`} alt="product.img" width="600px"></img>
            </>
            : '';
        return (
            <>
                <h2>{this.props.title}</h2>
                <form className="form-group" onSubmit={this.handleSubmit}>
                    <label>name</label>
                    <input className="form-control" value={this.state.data.name} type="text" name="name" placeholder="name" onChange={this.handleChange}></input>
                    <label>category</label>
                    <input className="form-control" value={this.state.data.category} type="text" name="category" placeholder="category" onChange={this.handleChange}></input>
                    <label>description</label>
                    <input className="form-control" value={this.state.data.description} type="text" name="description" placeholder="description" onChange={this.handleChange}></input>
                    <label>brand</label>
                    <input className="form-control" value={this.state.data.brand} type="text" name="brand" placeholder="brand" onChange={this.handleChange}></input>
                    <label>price</label>
                    <input className="form-control" value={this.state.data.price} type="number" name="price" placeholder="price" onChange={this.handleChange}></input>
                    <label>color</label>
                    <input className="form-control" value={this.state.data.color} type="text" name="color" placeholder="color" onChange={this.handleChange}></input>
                    <label>tags</label>
                    <input className="form-control" value={this.state.data.tags} type="text" name="tags" placeholder="tags" onChange={this.handleChange}></input>
                    <label>manuDate</label>
                    <input className="form-control" value={this.state.data.manuDate} type="date" name="manuDate" onChange={this.handleChange}></input>
                    <label>expiry date</label>
                    <input className="form-control" value={this.state.data.expiryDate} type="date" name="expiryDate" onChange={this.handleChange}></input>
                    <input type="checkbox" name="discountedItem" checked={this.state.data.discountedItem} onChange={this.handleChange}></input>
                    <label>Discounted Item</label>
                    {discountContent}
                    <br></br>
                    {preImage}
                    <label>Choose Image</label>
                    <br></br>
                    <input type="file" name="new_image" onChange={this.handleChange} ></input>
                    {btn}
                </form>
            </>
        )
    }
}


export const ProductForm = withRouter(ProductFormComponent);