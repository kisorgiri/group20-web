import React, { Component } from 'react'
import httpClient from '../../../utils/httpClient';
import { Loader } from './../../common/loader/loader.component';
import { Link } from 'react-router-dom';
import notification from '../../../utils/notification';
import { connect } from 'react-redux';
import { fetch_product, setPageNumber } from './../../../actions/product.actions';
const IMG_URL = process.env.REACT_APP_IMG_URL;


class ViewProductComponent extends Component {

    deleteProduct(id, index) {
        // eslint-disable-next-line no-restricted-globals
        let confirmed = confirm('Are you sure to remove?');
        // TODO use proper modal for confiramtion
        if (confirmed) {
            // this.setState({
            //     isDeleting: true
            // })
            httpClient.DELETE(`/product/${id}`, true)
                .then(data => {
                    notification.showInfo("product deleted");
                    const { products } = this.state;
                    products.splice(index, 1);
                    this.setState({ products })
                })
                .catch(err => {
                    notification.handleError(err);
                })
                .finally(() => {
                    // this.setState({
                    //     isDeleting: false
                    // })
                })
        }
    }
    previous = (e) => {
        let pageNumber = this.props.pageNumber - 1;
        this.props.fetch(pageNumber);
        this.props.changePageNumber(pageNumber);
    }
    next = (e) => {
        let pageNumber = this.props.pageNumber + 1;
        this.props.fetch(pageNumber);
        this.props.changePageNumber(pageNumber);
    }
    componentDidMount() {
        console.log('here check props >>', this.props);
        this.props.fetch();
        if (this.props.incomingData) {
            // TODO
        }
    }
    render() {
        let tableContent = (this.props.products || []).map((item, i) => (
            <tr key={item._id}>
                <td>{i + 1}</td>
                <td>{item.name}</td>
                <td>{item.category}</td>
                <td>{item.brand}</td>
                <td>{item.color}</td>
                <td>{item.price}</td>
                <td>
                    <img src={`${IMG_URL}/${item.image}`} alt="product.img" width="200px"></img>
                </td>
                <td>
                    <button className="btn btn-info">
                        <Link to={`edit-product/${item._id}`}>edit</Link>
                    </button>
                    <button onClick={() => this.deleteProduct(item._id, i)} className="btn btn-danger">delete</button>
                </td>
            </tr>
        ))
        let mainContent = this.props.isLoading
            ? <Loader></Loader>
            : <table className="table table-bordered">
                <thead>
                    <tr>
                        <th>S.N</th>
                        <th>Name</th>
                        <th>Category</th>
                        <th>Brand</th>
                        <th>Color</th>
                        <th>Price</th>
                        <th>Images</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {tableContent}
                    <button className="btn btn-info" onClick={this.previous}>Previous </button>
                    <button className="btn btn-info" onClick={this.next}> Next</button>
                </tbody>
            </table>
        return (
            <>
                <h2>View Product</h2>
                {mainContent}
            </>
        )
    }
}

// what components takes in
const mapStateToProps = state => ({
    products: state.product.products,
    isLoading: state.product.isLoading,
    pageNumber: state.product.pageNumber
})
// what component can dispatch
const mapDispatchToProps = dispatch => ({
    fetch: (pageNumber, pageSize) => dispatch(fetch_product(pageNumber, pageSize)),
    changePageNumber: (pageNumber) => dispatch(setPageNumber(pageNumber)),
})

export const ViewProduct = connect(mapStateToProps, mapDispatchToProps)(ViewProductComponent);
// connect({mapstate to props},{mapdispatchtoprops})(component)
