import React, { Component } from 'react'
import httpClient from '../../../utils/httpClient';
import notification from '../../../utils/notification';
import { ViewProduct } from '../view-products/view-products.component';

const defaultForm = {
    name: '',
    category: '',
    description: '',
    brand: '',
    color: '',
    minPrice: '',
    maxPrice: '',
    fromDate: '',
    multipleDateRange: false,
    toDate: '',
    discountedItem: '',
    tags: ''
}

export default class SearchProduct extends Component {

    constructor() {
        super();
        this.state = {
            data: {
                ...defaultForm
            },
            products: [],
            categories: [],
            error: {
                ...defaultForm
            },
            names: [],
            searchResult: [],
            isSubmitting: false,
            isValidForm: false,
            isLoading: false
        }
    }
    componentDidMount() {
        this.setState({
            isLoading: true
        })
        httpClient.POST('/product/search', {})
            .then(data => {
                let categories = [];
                data.data.forEach((item) => {
                    if (categories.indexOf(item.category) === -1) {
                        categories.push(item.category);
                    }
                });
                this.setState({
                    categories,
                    products: data.data
                })
            })
            .catch(err => {
                notification.handleError(err.response);
            })
            .finally(() => {
                this.setState({
                    isLoading: false
                })
            })
    }
    handleChange = e => {
        let { name, value, type, checked } = e.target;
        if (type === 'checkbox') {
            value = checked
        }
        if (name === 'category') {
            this.filterNames(value);
        }
        this.setState((pre) => ({
            data: {
                ...pre.data,
                [name]: value

            }
        }), () => {
            // form validation logic here make different function
        })
    }

    filterNames(categoryName) {
        let namesOnly = this.state.products.filter((item) => item.category === categoryName);
        this.setState({
            names: namesOnly
        })
    }
    handleSubmit = e => {
        e.preventDefault();
        this.setState({
            isSubmitting: true
        })

        // http call
        httpClient.POST('/product/search', this.state.data)
            .then(data => {
                // show it in view component
                if (data.data.length) {
                    this.setState({
                        searchResult: data.data
                    })
                }
                else {
                    notification.showInfo('No any Product matched your search query');
                }
            })
            .catch(err => {
                notification.handleError(err);
            })
            .finally(() => {
                this.setState({
                    isSubmitting: false
                })
            })
    }

    resetSearch = e => {
        this.setState({
            data:{...defaultForm},
            searchResult: []
        });
    }


    render() {
        let btn = this.state.isSubmitting
            ? <button className="btn btn-info" disabled={true}>searching...</button>
            : <button className="btn btn-primary" type="submit">search</button>
        let categoryOptions = this.state.categories.map((cat, i) => (
            <option key={i} value={cat}>{cat}</option>
        ));
        let nameOptions = this.state.names.map((item, i) => (
            <option key={item._id} value={item.name}>{item.name}</option>
        ));
        let nameContent = this.state.data.category
            ? <>
                <label>name</label>
                <select className="form-control" name="name" onChange={this.handleChange}>
                    <option selected={true} disabled={true}>(Select Name)</option>
                    {nameOptions}
                </select>
            </>
            : '';
        let toDateContent = this.state.data.multipleDateRange
            ? <>
                <label>To Date</label>
                <input className="form-control" type="date" name="toDate" onChange={this.handleChange}></input>
            </>
            : ''

        let mainContent = this.state.searchResult.length
            ? <>
                <button className="btn btn-success" onClick={this.resetSearch}>Search Again</button>
                <ViewProduct incomingData={this.state.searchResult}></ViewProduct>
            </>
            :
            <>
                <h2>Search Product</h2>
                <form className="form-group" onSubmit={this.handleSubmit}>
                    <label>Category</label>
                    <select className="form-control" name="category" onChange={this.handleChange}>
                        <option selected={true} disabled={true}>(Select Category)</option>
                        {categoryOptions}
                    </select>
                    {nameContent}
                    <label>Brand</label>
                    <input className="form-control" type="text" name="brand" placeholder="brand" onChange={this.handleChange}></input>
                    <label>Min Price</label>
                    <input className="form-control" type="number" name="minPrice" placeholder="0.0" onChange={this.handleChange}></input>
                    <label>Max Price</label>
                    <input className="form-control" type="number" name="maxPrice" placeholder="0.0" onChange={this.handleChange}></input>
                    <label>Date</label>
                    <input className="form-control" type="date" name="fromDate" onChange={this.handleChange}></input>
                    <input type="checkbox" onChange={this.handleChange} name="multipleDateRange"></input>
                    <label>multiple Date range</label>
                    <br></br>
                    {toDateContent}
                    <label>color</label>
                    <input className="form-control" type="text" name="color" placeholder="color" onChange={this.handleChange}></input>
                    <label>tags</label>
                    <input className="form-control" type="text" name="tags" placeholder="tags" onChange={this.handleChange}></input>
                    <br></br>
                    {btn}
                </form>
            </>;

        return (
            <>
                {mainContent}
            </>
        )
    }
}
