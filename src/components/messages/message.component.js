import React, { Component } from 'react'
import * as io from 'socket.io-client';
export default class MessageComponent extends Component {
    socket;
    constructor() {
        super();
        this.state = {
            data: {
                message: '',
                senderName: '',
                senderId: '',
                receiverId: '',
                time: ''
            },
            messges: [],
        };
    }

    handleChange = e => {
        const { name, value } = e.target;
        this.setState((pre) => ({
            data: {
                ...pre.data,
                [name]: value
            }
        }))
    }

    handleSubmit = (e) => {
        e.preventDefault();
        // data prepration
        // put seelcted user name and id in data>> receiver
        let data = {
            sender: "kishor",
            time: new Date().getTime(),
            message: this.state.data.message
        }
        this.socket.emit('new-msg', data);
    }

    componentDidMount() {
        // this.socket='';
        this.socket = io('http://localhost:8081');
        this.runSocket();
    }

    runSocket() {
        this.socket.emit('from-fe', 'hi from react');
        this.socket.on('from-be', (data) => {
            console.log('message from BE >>', data);
        })
        this.socket.on('reply-msg', (data) => {
            let { messges } = this.state;
            messges.push(data);
            this.setState({
                messges
            });
        })
    }



    render() {
        let content = this.state.messges.map((item, i) => (

            <li key={i}>
                <p>{item.message}</p>
                <p>{item.time}</p>
                <p>{item.sender}</p>
            </li>
        ))
        return (
            <div>
                <p>Chat form</p>
                <ul>
                    {content}
                </ul>
                <p>message input</p>
                <p>user list form</p>
                <form className="form-group" onSubmit={this.handleSubmit}>
                    <input className="form-control" placeholder="your message here" type="text" name="message" onChange={this.handleChange}></input>

                    <button className="btn btn-info" type="submit">send</button>
                </form>
            </div>
        )
    }
}



