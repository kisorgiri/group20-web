import React from 'react';
import { Link } from 'react-router-dom';
import httpClient from '../../utils/httpClient';
import notification from '../../utils/notification';
const defaultForm = {
    name: null,
    username: null,
    password: null,
    email: null,
    dob: null,
    gender: null,
    phoneNumber: null
};
export class Register extends React.Component {

    constructor() {
        super();
        this.state = {
            data: { ...defaultForm },
            error: { ...defaultForm },
            isSubmittig: false,
            isValidForm: false,
        };
    }
    componentDidMount() {
    }


    handleSubmit = (e) => {
        e.preventDefault();
        this.setState({
            isSubmittig: true
        })
        httpClient.POST('/auth/register', this.state.data)
            .then(data => {
                notification.showInfo("Registration Successfull please login");
                this.props.history.push('/');
            })
            .catch((err) => notification.handleError(err))
            .finally(() => {
                this.setState({
                    isSubmittig: false
                });
            });
    }
    handleChange = e => {
        const { name, value } = e.target;
        this.setState((previousState) => ({
            data: {
                ...previousState.data,
                [name]: value
            }
        }), () => {
            // validate form now
            this.validateForm(name);
        })
    }

    validateForm(fieldName) {
        let errorMsg;
        switch (fieldName) {
            case 'username':
                errorMsg = this.state.data[fieldName].length
                    ? ''
                    : 'Username is required'
                break;
            case 'email':
                errorMsg = this.state.data[fieldName].length
                    ? this.state.data[fieldName].includes('@')
                        ? ''
                        : 'Invalid Email Address'
                    : 'Email is required'
                break;
            default:
                break;
        }

        this.setState((pre) => ({
            error: {
                ...pre.error,
                [fieldName]: errorMsg
            }
        }), () => {
            this.validForm();
        })
    }
    validForm() {
        const errors = Object
            .values(this.state.error)
            .filter(err => err);
        let validForm = errors.length === 0;
        this.setState({
            isValidForm: validForm
        })
    }

    render() {
        let btn = this.state.isSubmittig
            ? <button disabled={true} className="btn btn-info" type="submit">submitting...</button>
            : <button disabled={!this.state.isValidForm} className="btn btn-primary" type="submit">Submit</button>


        return (
            <div>
                <h2>Register</h2>
                <p>Please Register to start using our application</p>
                <form className="form-group" onSubmit={this.handleSubmit}>
                    <label htmlFor="name"> Name</label>
                    <input className="form-control" type="text" name="name" placeholder="Name" id="name" onChange={this.handleChange} required></input>
                    <label htmlFor="email"> Email</label>
                    <input className="form-control" type="email" name="email" placeholder="Email" id="email" onChange={this.handleChange} required></input>
                    <p className="danger">{this.state.error.email}</p>

                    <label htmlFor="phoneNumber"> Phone Number</label>
                    <input className="form-control" type="number" name="phoneNumber" placeholder="Phone Number" id="phoneNumber" onChange={this.handleChange}></input>
                    <label htmlFor="username"> Username</label>
                    <input className="form-control" type="text" name="username" placeholder="Username" id="username" onChange={this.handleChange}></input>
                    <p className="danger">{this.state.error.username}</p>
                    <label htmlFor="password"> Password</label>
                    <input className="form-control" type="text" name="password" placeholder="Password" id="password" onChange={this.handleChange}></input>
                    <label> Gender</label>
                    <br />
                    <input type="radio" name="gender" value="male" onChange={this.handleChange} />Male
                    <br />
                    <input type="radio" name="gender" value="female" onChange={this.handleChange} />Female
                    <br />
                    <input type="radio" name="gender" value="others" onChange={this.handleChange} />Others
                    <br />

                    <label htmlFor="dob"> Date Of Birth</label>
                    <input className="form-control" type="date" name="dob" id="dob" onChange={this.handleChange}></input>
                    <br />
                    {btn}
                </form>
                <p>back to <Link to="/">login</Link></p>
            </div>
        )
    }
}