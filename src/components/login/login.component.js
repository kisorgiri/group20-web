// class based Component
import axios from 'axios';
import { Link } from 'react-router-dom';
import React, { Component } from 'react';
import notification from './../../utils/notification';
import httpClient from '../../utils/httpClient';
const defaultForm = {
    username: '',
    password: '',
}
export class Login extends Component {
    constructor() {
        super();
        this.state = {
            data: {
                ...defaultForm,
            },
            error: {
                ...defaultForm
            },
            remember_me: false,
            isSubmitting: false,
            validForm: false
        };
    }

    componentDidMount() {
        if (localStorage.getItem('rememberMe')) {
            this.props.history.push('/dashboard');
        }
    }

    handleChange(e) {
        let { name, value } = e.target;
        // state modify
        this.setState((pre) => ({
            data: {
                ...pre.data,
                [name]: value
            }
        }), () => {
            this.validateForm(name);
        })
    }
    validateForm(fieldName) {
        let errMsg = this.state.data[fieldName].length
            ? ''
            : ` ${fieldName} is required`;

        this.setState(pre => ({
            error: {
                ...pre.error,
                [fieldName]: errMsg
            }
        }), () => {
            const errors = Object
                .values(this.state.error)
                .filter(err => err);
            let errExist = errors.length > 0;
            this.setState({
                validForm: !errExist
            })

        })
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.setState({
            isSubmitting: true
        })
        httpClient.POST('/auth/login', this.state.data)
            .then((data) => {
                // web storage
                localStorage.setItem('token', data.data.token);
                localStorage.setItem('user', JSON.stringify(data.data.user));

                this.props.history.push('/dashboard');
            })
            .catch(err => {
                notification.handleError(err.response);
            })
            .finally(() => {
                this.setState({
                    isSubmitting: false
                })
            })

    }

    rememberMe = (e) => {
        // logic
        let remember = e.target.checked;

        localStorage.setItem('rememberMe', remember);
    }

    render() {
        let btn = this.state.isSubmitting
            ? <button disabled={true} className="btn btn-info">Logging in...</button>
            : <button disabled={!this.state.validForm} onClick={this.handleSubmit} className="btn btn-primary">Login</button>
        return (
            <div>
                <h2>Login</h2>
                <p>Please Login to Continue your session</p>
                <form className="form-group">
                    <label htmlFor="username">Username</label>
                    <input className="form-control" id="username" type="text" onChange={this.handleChange.bind(this)} placeholder="Username" name="username"></input>
                    <p className="danger">{this.state.error.username}</p>
                    <label htmlFor="password">Password</label>
                    <input className="form-control" id="password" type="password" onChange={this.handleChange.bind(this)} placeholder="Password" name="password"></input>
                    <p className="danger">{this.state.error.password}</p>
                    <input type="checkbox" name="rememeberMe" onChange={this.rememberMe.bind(this)} /> Remember Me
                    <br></br>
                    {btn}
                </form>
                <p>Don't have an account?</p>
                <p>Register <Link to="/register"> here</Link></p>
                <p><Link to="/forgot-password">forgot password?</Link></p>
            </div>
        )
    }
}
