import React from 'react';
import AppRoutes from './app.routing';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import store from './store';
import { Provider } from 'react-redux';
// component is main gist of react
// component are part of your application view

// component can be functional component
// component can be class based component

// component can be stateful 
// ====> class based component are statefull component
// componet can be state less
// ==> usually functional componnet are stateless

// functional component
export const App = (props) => {
    return (
        <div>
            <ToastContainer></ToastContainer>
            <Provider store={store}>
                <AppRoutes />
            </Provider>
        </div>

    )
}