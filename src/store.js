import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import rootReducers from './reducers';
// combined reducers

const initialState = {};
const middlewares = [thunk];

const store = createStore(
    rootReducers,
    initialState,
    applyMiddleware(...middlewares)
);

export default store;

